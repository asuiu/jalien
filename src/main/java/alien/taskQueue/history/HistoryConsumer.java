package alien.taskQueue.history;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import alien.config.ConfigUtils;

/**
 * @author Elena Mihailescu
 * @since 2024-08-20
 */
public abstract class HistoryConsumer implements Runnable {
	private static final int queueSize = 1024;
	static transient final Logger logger = ConfigUtils.getLogger(HistoryConsumer.class.getCanonicalName());

	// Queue all requests
	final BlockingQueue<Map<String, Object>> queue;

	/**
	 * Default constructor
	 */
	public HistoryConsumer() {
		this.queue = new LinkedBlockingQueue<>(queueSize);
	}

	/**
	 * @param data
	 */
	public void offer(Map<String, Object> data) {
		queue.offer(data);
	}

	/**
	 * @param data
	 */
	public abstract void processData(Map<String, Object> data);

	public abstract Long getEstimatedTTL(Map<String, Object> request);

	@Override
	public void run() {
		while (true) {
			final Map<String, Object> fields;
			// Block until we have something to process
			try {
				fields = queue.take();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
				break;
			}

			if (fields == null)
				continue;

			// Process data
			processData(fields);
		}
	}
}
