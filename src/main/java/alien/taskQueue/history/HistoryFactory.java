package alien.taskQueue.history;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import alien.config.ConfigUtils;

/**
 * @author Elena Mihailescu
 * @since 2024-08-20
 */
public class HistoryFactory {
	private final Collection<HistoryConsumer> historyConsumers;
	static transient final Logger logger = ConfigUtils.getLogger(HistoryFactory.class.getCanonicalName());

	/**
	 * Default constructor
	 */
	public HistoryFactory() {
		this.historyConsumers = new ArrayList<>();
	}

	/**
	 * @param consumer
	 */
	public void addConsumer(final HistoryConsumer consumer) {
		logger.log(Level.INFO, "Add consumer: " + consumer);
		this.historyConsumers.add(consumer);
		final Thread t = new Thread(consumer, consumer.getClass().getCanonicalName());
		t.setDaemon(true);
		t.start();
	}

	/**
	 * @param consumer
	 */
	public void removeConsumer(final HistoryConsumer consumer) {
		logger.log(Level.INFO, "Remove consumer: " + consumer);
		this.historyConsumers.remove(consumer);
	}

	/**
	 * @param data
	 */
	public void offer(final Map<String, Object> data) {
		// Create new object
		final HashMap<String, Object> newData = new HashMap<>(data);

		// Same object is added in each queue
		for (final HistoryConsumer historyConsumer : historyConsumers) {
			historyConsumer.offer(newData);
		}
	}
}
