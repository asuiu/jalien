package alien.api.taskQueue;

import java.util.Arrays;
import java.util.List;

import alien.api.Request;
import alien.taskQueue.TaskQueueUtils;

/**
 * Add instances to debug remotely
 *
 * @author marta
 * @since
 */
public class AddRemoteDebugging extends Request {
	/**
	 *
	 */
	private static final long serialVersionUID = 5022083696413315513L;

	private int instances;
	private String site;
	private String host;
	private long buildTs;

	private String remoteDebuggingStatus;

	/**
	 * @param instancesToDebug
	 * @param sitename
	 * @param hostname
	 * @param ts
	 */
	public AddRemoteDebugging(int instancesToDebug, String sitename, String hostname, long ts) {
		instances = instancesToDebug;
		site = sitename;
		host = hostname;
		buildTs = ts;
	}

	@Override
	public List<String> getArguments() {
		return Arrays.asList();
	}

	@Override
	public void run() {
		remoteDebuggingStatus = TaskQueueUtils.addRemoteDebugging(instances, site, host, buildTs);

	}

	/**
	 * @return whether or not this instance should send logs to central services
	 */
	public String getAdditionSuccess() {
		return remoteDebuggingStatus;
	}

	@Override
	public String toString() {
		return "";
	}
}
